# !/bin/bash

function usage {
        echo "./$(basename $0) -h --> shows usage"
}

function abspath() {
    # generate absolute path from relative path
    # $1     : relative filename
    # return : absolute path
    if [ -d "$1" ]; then
        # dir
        (cd "$1"; pwd)
    elif [ -f "$1" ]; then
        # file
        if [[ $1 = /* ]]; then
            echo "$1"
        elif [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}

# if no input argument found, exit the script with usage
if [[ ${#} -eq 0 ]]; then
   usage
   exit 1
fi

# Define list of arguments expected in the input
optstring="o:d:"
# optstring="i:o:"

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    o)
      output_arg="${OPTARG}"
      ;;
    d)
      date_arg="${OPTARG}"
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      echo
      usage
      exit 1
      ;;
  esac
done

shift "$(($OPTIND -1))"

# if [[ -n "$output" ]] ; then
# 	echo "Output included"
# 	exit
# fi


input=$(abspath "$1")

if [[ ! -f "$input" && ! -d "$input" ]] ; then
	echo "Input path does not exist"
	echo "Input path value: $input"
	usage
	exit 1
fi

# Replace spaces in filenames with underscores
if [[ -d "$input" ]] ; then
	for file in "$input"/* ; do
		filename=$(basename "$file")
		filename_formatted="${filename// /_}"
		filepath_formatted="${input}/${filename_formatted}"
		mv "$file" "$filepath_formatted"
	done
fi

if [[ -f $(abspath "$output_arg") ]] ; then
	echo "Argument for output (-o) should be a directory"
	echo "Output (-o) value: $output_arg"
	usage
	exit 1
fi

if [[ -n "$output_arg" ]] ; then
	if $(mkdir -p "$output_arg") ; then
		output=$(abspath "$output_arg")
		if [[ -d "$input" ]] ; then
			cp -a "${input}/." "${output}/."
		elif [[ -f "$input" ]] ; then
			cp "$input" "${output}/."
		else
			echo "Input path is neither a file or directory"
		fi
	else
		echo "Unable to create output (-o) directory"
		exit 1
	fi
else
  output="$input"
fi

json_files=$(find "$output" -type f -name "*.json")

for file in $json_files ; do
	directory=$(dirname "$file")
	filename_without_json_extension=$(basename "${file%.json}")
	filename_without_extension=${filename_without_json_extension%.*}
	extension_unparsed=${filename_without_json_extension##*.}
	extension=${extension_unparsed%(*}
	parenthesis=$(echo "$extension_unparsed" | grep -Eo '\(.*\)')
	if [[ -f "${directory}/${filename_without_extension}-edited.${extension}" ]] ; then
		cp -n "$file" "${directory}/${filename_without_extension}-edited.${extension}.json"
	fi
	if [[ -f "${directory}/${filename_without_extension}${parenthesis}.${extension}" && -n "$parenthesis" ]] ; then
		mv -n "$file" "${directory}/${filename_without_extension}${parenthesis}.${extension}.json"
	fi
done

media_files=$(find "$output" -type f ! -name "*.json" ! -name ".*")

for file in $media_files ; do
	directory=$(dirname "$file")
	filename_without_extension=$(basename "$file" | cut -d '.' -f 1)
	extension=$(basename "$file" | cut -d '.' -f 2)
	if [[ -f "${directory}/${filename_without_extension}.json" ]] ; then
		cp -n "${directory}/${filename_without_extension}.json" "${directory}/${filename_without_extension}.${extension}.json"
	fi
	if [[ -f "${directory}/${filename_without_extension}..json" ]] ; then
		cp -n "${directory}/${filename_without_extension}..json" "${directory}/${filename_without_extension}.${extension}.json"
	fi
	if [[ -f "${directory}/${filename_without_extension%?}.json" ]] ; then
		cp -n "${directory}/${filename_without_extension%?}.json" "${directory}/${filename_without_extension}.${extension}.json"
	fi
done

if [[ -n "$date_arg" ]] ; then

  echo -e "\n\n JPG - Rewrite DateTimeOriginal Tag \n\n"
  exiftool -config config/exiftool.conf \
    -if '$Filetype eq "JPEG"' \
    --ext json -v0 -r \
    -overwrite_original_in_place \
    "-MWG:DateTimeOriginal=${date_arg}" \
    "$output"

  echo -e "\n\n JPG - Rewrite CreateDate & ModifyDate Tags \n\n"
  exiftool -config config/exiftool.conf \
    -if '$Filetype eq "JPEG"' \
    --ext json -v0 -r \
    -overwrite_original_in_place \
    '-MWG:CreateDate<MWG:DateTimeOriginal' \
    '-ModifyDate<MWG:DateTimeOriginal' \
    "$output"

  echo -e "\n\n Rename File(s) \n\n"
  exiftool -config config/exiftool.conf -@ args/rename.args "$output"

  exit 0
fi


echo -e "\n\nGPS - Clear Tags Equal to Zero\n\n"
exiftool -config config/exiftool.conf -@ args/gps_clear_blank_tags.args "$output"

echo -e "\n\nGPS - Use JSON Data\n\n"
exiftool -config config/exiftool.conf -@ args/gps_use_json.args "$output"

echo -e "\n\nUse Misc JSON Data\n\n"
exiftool -config config/exiftool.conf -@ args/misc_use_json.args "$output"

exit

echo -e "\n\nBackup DateTimeOriginal Tag\n\n"
exiftool -config config/exiftool.conf -@ args/backup_datetimeoriginal.args "$output"

echo -e "\n\nStore Date & Time from JSON as DateTimeOriginal\n\n"
exiftool -config config/exiftool.conf -@ args/store_date_from_json.args "$output"

echo -e "\n\nStore Date & Time from DateTimeOriginal (from JSON) as JSONDateTime\n\n"
exiftool -config config/exiftool.conf -@ args/store_datetimeoriginal_as_jsondatetime.args "$output"

echo -e "\n\nRestore Date & Time from DateTimeOriginalBackup\n\n"
exiftool -config config/exiftool.conf -@ args/restore_datetimeoriginal.args "$output"

echo -e "\n\nBackup DateTimeOriginal Tag\n\n"
exiftool -config config/exiftool.conf -@ args/backup_datetimeoriginal.args "$output"

echo -e "\n\nStore Date & Time from Filename as DateTimeOriginal\n\n"
exiftool -config config/exiftool.conf -@ args/store_date_from_filename.args "$output"

echo -e "\n\nStore Date & Time from DateTimeOriginal (from Filename) as FileNameDateTime\n\n"
exiftool -config config/exiftool.conf -@ args/store_datetimeoriginal_as_filenamedatetime.args "$output"

echo -e "\n\nRestore Date & Time from DateTimeOriginalBackup\n\n"
exiftool -config config/exiftool.conf -@ args/restore_datetimeoriginal.args "$output"

echo -e "\n\nBackup Primary Date & Time Tags\n\n"
exiftool -config config/exiftool.conf -@ args/backup_datetime.args "$output"

# echo -e "\n\nJPG - Write Date & Time to Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/jpg_date.args "$output"
#
# echo -e "\n\nPNG - Write Date & Time to Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/png_date.args "$output"
#
# echo -e "\n\nGIF - Write Date & Time to Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/gif_date.args "$output"

# echo -e "\n\nMP4 - Write Date & Time to Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/mp4_date.args "$output"
#
# echo -e "\n\nMP4 - Rewrite Date & Time to Tags - Fix FileNameDateTime is in the Future\n\n"
# exiftool -config config/exiftool.conf -@ args/mp4_date_future_filenamedatetime.args "$output"

echo -e "\n\nRename Files\n\n"
exiftool -config config/exiftool.conf -@ args/rename.args "$output"

exit

echo -e "\n\nJPG - Use GPS Date & Time if Possible\n\n"
exiftool -config config/exiftool.conf -@ args/jpg_date_use_gps.args "$output"

echo -e "\n\nJPG - Use JSON Date & Time if Needed\n\n"
exiftool -config config/exiftool.conf -@ args/jpg_date_use_json.args "$output"

echo -e "\n\nJPG - Use Filename Date & Time if Needed\n\n"
exiftool -config config/exiftool.conf -@ args/jpg_date_use_filename.args "$output"

echo -e "\n\nPNG - Use JSON Date & Time if Needed\n\n"
exiftool -config config/exiftool.conf -@ args/png_date_use_json.args "$output"

echo -e "\n\nPNG - Use Filename Date & Time if Needed\n\n"
exiftool -config config/exiftool.conf -@ args/png_date_use_filename.args "$output"

echo -e "\n\nGIF - Use JSON Date & Time if Needed\n\n"
exiftool -config config/exiftool.conf -@ args/gif_date_use_json.args "$output"
# echo -e "\n\nMP4/MOV - Replicate Quicktie:CreateDate to Other Date/Time Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/mp4_date_use_createdate.args "$output"
echo -e "\n\nMP4/MOV - Use JSON Date & Time if Needed\n\n"
exiftool -config config/exiftool.conf -@ args/mp4_date_use_json.args "$output"
# echo -e "\n\nClean up Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/tag_cleanup.args "$output"
echo -e "\n\nRename Files\n\n"
exiftool -config config/exiftool.conf -@ args/rename.args "$output"



exit

exiftool -@ args/gps_use_json.args "$output"
exiftool -@ args/misc_use_json.args "$output"
exiftool -config config/exiftool.conf -@ args/jpg_date_use_json.args "$output"
exiftool -@ args/png_date_use_json.args "$output"
exiftool -@ args/gif_date_use_json.args "$output"
exiftool -@ args/mp4_date_use_json.args "$output"
exiftool -@ args/mp4_date_use_createdate.args "$output"
exiftool -@ args/rename.args "$output"

exit


# for file in "$originals_directory"/* ; do
# 	filename=$(basename "$file")
# 	filename_formatted=$(echo "$filename" | tr '[:upper:]' '[:lower:]')
# 	filename_formatted=$(echo "$filename_formatted" | tr -d ' ')
# 	filepath_formatted="${originals_directory}/${filename_formatted}"
# 	mv "$file" "$filepath_formatted"
# done
#
# for file in "$originals_directory"/*.jpg ; do
# 	echo "${originals_directory}/$(basename "$file" .jpg).jpeg"
# 	mv -n "$file" "${originals_directory}/$(basename "$file" .jpg).jpeg"
# done
#
# for file in "$exported_directory"/* ; do
# 	filename=$(basename "$file")
# 	filename_formatted=$(echo "$filename" | tr '[:upper:]' '[:lower:]')
# 	filename_formatted=$(echo "$filename_formatted" | tr -d ' ')
# 	filepath_formatted="${exported_directory}/${filename_formatted}"
# 	mv "$file" "$filepath_formatted"
# done
#
# Move all except .jpg files to export folder
# originals_to_move="$(find "$originals_directory" -type f ! -name "*.jpg" ! -name ".*")"
# originals_to_move="$(find "$originals_directory" -type f ! -name "*.jpg" ! -name "*.jpeg" ! -name ".*")"
#
# for file in $originals_to_move ; do
# 	mv -n "$file" "$exported_directory/$(basename "$file")"
# done
#
# Fixup exif/iptc data
# exiftool -config config/exiftool.conf -overwrite_original --ext xmp '-IPTC:CodedCharacterSet=UTF8' -tagsFromFile %d%f.xmp -@ args/xmp2exif.args '-GPS:GPSLatitudeRef<XMP:GPSLatitudeRef' '-GPS:GPSLongitudeRef<XMP:GPSLongitudeRef' -@ args/xmp2iptc.args '-XMP-photoshop:DateCreated<XMP-photoshop:DateCreated' '-FileCreateDate<XMP-photoshop:DateCreated' '-AllDates<XMP-photoshop:DateCreated' "$exported_directory"

exiftool -@ ~/code-personal/exiftool-scripts-for-takeout/args/use_json.args workspace
exiftool -@ ~/code-personal/exiftool-scripts-for-takeout/args/jpg_to_mp4.args workspace
exiftool -@ ~/code-personal/exiftool-scripts-for-takeout/args/jpg_to_png.args workspace
exiftool -@ ~/code-personal/exiftool-scripts-for-takeout/args/png_to_jpg.args workspace
exiftool -@ ~/code-personal/exiftool-scripts-for-takeout/args/was_jpg_now_mp4.args workspace
exiftool -@ ~/code-personal/exiftool-scripts-for-takeout/args/was_jpg_now_png.args workspace
exiftool -@ ~/code-personal/exiftool-scripts-for-takeout/args/was_png_now_jpg.args workspace

# Rename files based on capture date/time
exiftool -r -v0 -d '%Y%m%d_%H%M%S%%-03.c.%%e' '-filename<DateTimeOriginal' workspace
# exiftool -config config/exiftool.conf -d '%Y%m%d_%H%M%S%%-03.c.%%e' '-filename<DateTimeOriginal' Takeout
