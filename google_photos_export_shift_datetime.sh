# !/bin/bash

if [[ -z $1 ]] ; then
	echo "Input file must be included"
	exit
fi

if [[ -z $2 ]] ; then
	echo "Date/Time shift value must be included, per exiftool format"
	exit
fi
# if [[ -z $2 ]] ; then
# 	echo "Output folder must be included"
# 	exit
# fi

function abspath() {
    # generate absolute path from relative path
    # $1     : relative filename
    # return : absolute path
    if [ -d "$1" ]; then
        # dir
        (cd "$1"; pwd)
    elif [ -f "$1" ]; then
        # file
        if [[ $1 = /* ]]; then
            echo "$1"
        elif [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}

input_file=$(abspath "$1")
shift_value=$2

if [[ "$shift_value" -lt 0 ]] ; then
	sign="-"
	shift_value=$(echo "$shift_value" | tr -d '-')
else
	sign="+"
fi

# Replace spaces in filenames with underscores
# for file in "$input_directory"/* ; do
# 	filename=$(basename "$file")
# 	filename_formatted="${filename// /_}"
# 	filepath_formatted="${input_directory}/${filename_formatted}"
# 	mv "$file" "$filepath_formatted"
# done
#
# cp -a "$input_directory/." "$output_directory/."
#
# json_files=$(find "$output_directory" -type f -name "*.json")
#
# for file in $json_files ; do
# 	directory=$(dirname "$file")
# 	filename_without_json_extension=$(basename "${file%.json}")
# 	filename_without_extension=${filename_without_json_extension%.*}
# 	extension_unparsed=${filename_without_json_extension##*.}
# 	extension=${extension_unparsed%(*}
# 	parenthesis=$(echo "$extension_unparsed" | grep -Eo '\(.*\)')
# 	if [[ -f "${directory}/${filename_without_extension}-edited.${extension}" ]] ; then
# 		cp -n "$file" "${directory}/${filename_without_extension}-edited.${extension}.json"
# 	fi
# 	if [[ -f "${directory}/${filename_without_extension}${parenthesis}.${extension}" ]] ; then
# 		cp -n "$file" "${directory}/${filename_without_extension}${parenthesis}.${extension}.json"
# 	fi
# done
#
# media_files=$(find "$output_directory" -type f ! -name "*.json" ! -name ".*")
#
# for file in $media_files ; do
# 	directory=$(dirname "$file")
# 	filename_without_extension=$(basename "$file" | cut -d '.' -f 1)
# 	extension=$(basename "$file" | cut -d '.' -f 2)
# 	if [[ -f "${directory}/${filename_without_extension}.json" ]] ; then
# 		cp -n "${directory}/${filename_without_extension}.json" "${directory}/${filename_without_extension}.${extension}.json"
# 	fi
# 	if [[ -f "${directory}/${filename_without_extension}..json" ]] ; then
# 		cp -n "${directory}/${filename_without_extension}..json" "${directory}/${filename_without_extension}.${extension}.json"
# 	fi
# 	if [[ -f "${directory}/${filename_without_extension%?}.json" ]] ; then
# 		cp -n "${directory}/${filename_without_extension%?}.json" "${directory}/${filename_without_extension}.${extension}.json"
# 	fi
# done

# echo -e "\n\nRewrite Corrupted EXIF\n\n"
# exiftool -all= -tagsfromfile @ -all:all -unsafe -icc_profile -overwrite_original_in_place "$input_file"

echo -e "\n\nGPS - Clear Tags Equal to Zero\n\n"
exiftool -config config/exiftool.conf -@ args/gps_clear_blank_tags.args "$input_file"

echo -e "\n\nJPG - Shift Time and/or Date\n\n"
exiftool -config config/exiftool.conf "-AllDates${sign}=${shift_value}" -overwrite_original_in_place "$input_file"

echo -e "\n\nJPG (MWG) - Write Date & Time to Tags\n\n"
exiftool -config config/exiftool.conf -@ args/jpg_date_mwg_after_adjustment.args "$input_file"

# echo -e "\n\nPNG - Write Date & Time to Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/png_date.args "$output_directory"
#
# echo -e "\n\nGIF - Write Date & Time to Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/gif_date.args "$output_directory"

# echo -e "\n\nMP4 - Write Date & Time to Tags\n\n"
# exiftool -config config/exiftool.conf -@ args/mp4_date.args "$output_directory"
#
# echo -e "\n\nMP4 - Rewrite Date & Time to Tags - Fix FileNameDateTime is in the Future\n\n"
# exiftool -config config/exiftool.conf -@ args/mp4_date_future_filenamedatetime.args "$output_directory"

echo -e "\n\nRename Files\n\n"
exiftool -config config/exiftool.conf -@ args/rename.args "$input_file"
