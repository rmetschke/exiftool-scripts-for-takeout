# !/bin/bash

function usage {
        echo "./$(basename $0) -h --> shows usage"
}

function abspath() {
    # generate absolute path from relative path
    # $1     : relative filename
    # return : absolute path
    if [ -d "$1" ]; then
        # dir
        (cd "$1"; pwd)
    elif [ -f "$1" ]; then
        # file
        if [[ $1 = /* ]]; then
            echo "$1"
        elif [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}

# if no input argument found, exit the script with usage
if [[ ${#} -eq 0 ]]; then
  usage
  exit 1
fi

# Define list of arguments expected in the input
optstring="o:l:L:c:C:s:"

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    o)
      output_arg="${OPTARG}"
      ;;
    l)
      latitude_arg="${OPTARG}"
      ;;
    L)
      longitude_arg="${OPTARG}"
      ;;
    c)
      city_arg="${OPTARG}"
      ;;
    C)
      country_arg="${OPTARG}"
      ;;
    s)
      state_arg="${OPTARG}"
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      echo
      usage
      exit 1
      ;;
  esac
done

shift "$(($OPTIND -1))"

if [[ -n "$latitude_arg" || -n "$longitude_arg" ]] ; then
  if [[ -z "$latitude_arg" || -z "$longitude_arg" ]] ; then
    echo "Latitude or longitude coordinates were not specified as floating point numbers."
    usage
    exit 1
  fi
fi

input=$(abspath "$1")
latitude=$latitude_arg
longitude=$longitude_arg
city=$city_arg
country=$country_arg
state=$state_arg

# echo -e "\n\nRewrite Corrupted EXIF\n\n"
# exiftool -all= -tagsfromfile @ -all:all -unsafe -icc_profile -overwrite_original_in_place "$input_file"

echo -e "\n\nGPS - Clear Tags Equal to Zero\n\n"
exiftool -config config/exiftool.conf -@ args/gps_clear_blank_tags.args "$input"
exiftool -config config/exiftool.conf -@ args/gps_xmp_clear_blank_tags.args "$input"

if [[ -n "$latitude" && -n "$longitude" ]] ; then
  echo -e "\n\n Set GPS Coordinates \n\n"
  exiftool -config config/exiftool.conf -@ args/gps_set_location.args "-GPSLatitude=${latitude}" "-GPSLatitudeRef=${latitude}" "-GPSLongitude=${longitude}" "-GPSLongitudeRef=${longitude}" -overwrite_original_in_place "$input"
fi

if [[ -n "$city" ]] ; then
  echo -e "\n\n Set City \n\n"
  exiftool -config config/exiftool.conf \
    -if '$Filetype eq "JPEG"' \
    --ext json -v0 -r \
    -overwrite_original_in_place \
    "-MWG:city=${city}" \
    "$input"
fi

if [[ -n "$state" ]] ; then
  echo -e "\n\n Set State \n\n"
  exiftool -config config/exiftool.conf \
    -if '$Filetype eq "JPEG"' \
    --ext json -v0 -r \
    -overwrite_original_in_place \
    "-MWG:State=${state}" \
    "$input"
fi

if [[ -n "$country" ]] ; then
  echo -e "\n\n Set Country \n\n"
  exiftool -config config/exiftool.conf \
    -if '$Filetype eq "JPEG"' \
    --ext json -v0 -r \
    -overwrite_original_in_place \
    "-MWG:Country=${country}" \
    "$input"
fi
