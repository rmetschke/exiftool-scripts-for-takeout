# !/bin/bash
# This script depends on ImageMagick and exiftool being installed already

png_files=""
return_code=""

function usage {
        echo "./$(basename $0) -h --> shows usage"
}

function abspath() {
    # generate absolute path from relative path
    # $1     : relative filename
    # return : absolute path
    if [ -d "$1" ]; then
        # dir
        (cd "$1"; pwd)
    elif [ -f "$1" ]; then
        # file
        if [[ $1 = /* ]]; then
            echo "$1"
        elif [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}

# if no input argument found, exit the script with usage
if [[ ${#} -eq 0 ]]; then
  usage
  exit 1
fi

input=$(abspath "$1")
# Currently, this does nothing, but put in place in for future use
output=$(abspath "$2")

if [[ ! -d "$input" ]] ; then
  echo "'$1' is not a directory"
  exit 1
fi

png_files=$(find "$input" -type f -name "*.png")

for file in $png_files ; do
  echo ""
  echo "===="
  echo "Converting $file to $(dirname "$file")/$(basename "$file" .png).jpg"
  magick "$file" "$(dirname "$file")/$(basename "$file" .png).jpg"
  return_code="$?"
  [[ "$return_code" -eq 0 ]] && rm -f "$file" || echo "Failed to convert '$file' to JPEG"
done
