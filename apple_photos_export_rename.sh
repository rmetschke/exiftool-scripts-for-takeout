#!/bin/bash

if [[ -z $1 ]] ; then
	echo "Unmodified originals folder must be included"
	exit
fi
if [[ -z $2 ]] ; then
	echo "Exported photos/videos folder must be included"
	exit
fi

function abspath() {
    # generate absolute path from relative path
    # $1     : relative filename
    # return : absolute path
    if [ -d "$1" ]; then
        # dir
        (cd "$1"; pwd)
    elif [ -f "$1" ]; then
        # file
        if [[ $1 = /* ]]; then
            echo "$1"
        elif [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}


originals_directory=$(abspath "$1")
exported_directory=$(abspath "$2")

original_files=$(find "$originals_directory")
exported_files=$(find "$exported_directory")


for file in "$originals_directory"/* ; do
	filename=$(basename "$file")
	filename_formatted=$(echo "$filename" | tr '[:upper:]' '[:lower:]')
	filename_formatted=$(echo "$filename_formatted" | tr -d ' ')
	filepath_formatted="${originals_directory}/${filename_formatted}"
	mv "$file" "$filepath_formatted"
done

for file in "$originals_directory"/*.xmp ; do
# 	echo "${originals_directory}/$(basename "$file" .jpg).jpeg"
	cp "$file" "${exported_directory}/$(basename "$file")"
done

for file in "$exported_directory"/* ; do
	filename=$(basename "$file")
	filename_formatted=$(echo "$filename" | tr '[:upper:]' '[:lower:]')
	filename_formatted=$(echo "$filename_formatted" | tr -d ' ')
	filepath_formatted="${exported_directory}/${filename_formatted}"
	mv "$file" "$filepath_formatted"
done

# exit

# Move all except .jpg files to export folder
# originals_to_move="$(find "$originals_directory" -type f ! -name "*.jpg" ! -name ".*")"
# originals_to_move="$(find "$originals_directory" -type f ! -name "*.jpg" ! -name "*.jpeg" ! -name ".*")"
#
# for file in $originals_to_move ; do
# 	mv -n "$file" "$exported_directory/$(basename "$file")"
# done

# Fixup exif/iptc data
# exiftool -config config/exiftool.conf -overwrite_original --ext xmp '-IPTC:CodedCharacterSet=UTF8' -tagsFromFile %d%f.xmp -@ args/xmp2exif.args '-GPS:GPSLatitudeRef<XMP:GPSLatitudeRef' '-GPS:GPSLongitudeRef<XMP:GPSLongitudeRef' -@ args/xmp2iptc.args '-XMP-photoshop:DateCreated<XMP-photoshop:DateCreated' '-FileCreateDate<XMP-photoshop:DateCreated' '-AllDates<XMP-photoshop:DateCreated' "$exported_directory"

# Rename files based on capture date/time
exiftool -config config/exiftool.conf -d '%Y%m%d_%H%M%S%%-03.c.%%e' -tagsFromFile %d%f.xmp '-filename<XMP-photoshop:DateCreated' "$exported_directory"

# Rename files based on capture date/time
exiftool -config config/exiftool.conf -d '%Y%m%d_%H%M%S%%-03.c.%%e' -tagsFromFile %d%f.xmp '-filename<XMP-photoshop:DateCreated' "$originals_directory"
